<?php

namespace spec\Amneale\Ledger;

use Amneale\Ledger\Description;
use Amneale\Ledger\Identity;
use PhpSpec\ObjectBehavior;
use Amneale\Ledger\Entity;
use Amneale\Ledger\Amount;
use Amneale\Ledger\Type;

class TransactionSpec extends ObjectBehavior
{
    /**
     * @var Amount
     */
    private $amount;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var Description
     */
    private $description;

    function let(\DateTime $date)
    {
        $this->amount = new Amount(100);
        $this->type = new Type(Type::CREDIT);
        $this->description = new Description('test description');

        $this->beConstructedThrough('create', [$this->amount, $this->type, $this->description, $date]);
    }

    function it_is_an_entity()
    {
        $this->shouldImplement(Entity::class);
    }

    function it_can_be_a_credit_transaction()
    {
        $this->isCredit()->shouldReturn(true);
    }

    function it_can_be_a_debit_transaction(\DateTime $date)
    {
        $this->beConstructedThrough(
            'create', [$this->amount, new Type(Type::DEBIT), $this->description, $date]
        );

        $this->isCredit()->shouldReturn(false);
    }

    function it_has_an_id()
    {
        $this->getId()->shouldReturnAnInstanceOf(Identity::class);
    }

    function it_has_an_amount()
    {
        $this->getAmount()->shouldReturn($this->amount);
    }

    function it_has_a_type()
    {
        $this->getType()->shouldReturn($this->type);
    }

    function it_has_a_date(\DateTime $date)
    {
        $this->getDate()->shouldReturn($date);
    }

    function it_has_a_description()
    {
        $this->getDescription()->shouldReturn($this->description);
    }
}
