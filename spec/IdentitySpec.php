<?php

namespace spec\Amneale\Ledger;

use PhpSpec\ObjectBehavior;
use Amneale\Ledger\Identity;

class IdentitySpec extends ObjectBehavior
{
    function it_should_be_unique()
    {
        $identity = new Identity();

        $this->asString()->shouldNotReturn($identity->asString());
    }

    function it_can_be_represented_as_a_string()
    {
        $this->asString()->shouldBeString();
    }

    function it_should_always_provide_the_same_string_value()
    {
        $this->asString()->shouldReturn($this->asString());
    }

    function it_is_initializable_with_an_identity_string()
    {
        $uuidString = '25769c6c-d34d-4bfe-ba98-e0ee856f3e7a';

        $this->beConstructedWith($uuidString);
        $this->asString()->shouldReturn($uuidString);
    }

    function it_is_not_initializable_with_an_invalid_identity_string()
    {
        $this->beConstructedWith('foobarbaz');
        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
