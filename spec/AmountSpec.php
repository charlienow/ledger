<?php

namespace spec\Amneale\Ledger;

use PhpSpec\ObjectBehavior;

class AmountSpec extends ObjectBehavior
{
    const TEST_AMOUNT = 100;

    function let()
    {
        $this->beConstructedWith(self::TEST_AMOUNT);
    }

    function it_can_be_represented_as_an_integer()
    {
        $this->asInt()->shouldReturn(self::TEST_AMOUNT);
    }

    function it_cannot_be_initialized_with_a_non_integer()
    {
        $this->beConstructedWith('one hundred');
        $this->shouldThrow(\TypeError::class)->duringInstantiation();
    }

    function it_cannot_be_initialized_with_a_zero_amount()
    {
        $this->beConstructedWith(0);
        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }

    function it_cannot_be_initialized_with_a_negative_amount()
    {
        $this->beConstructedWith(-self::TEST_AMOUNT);
        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
