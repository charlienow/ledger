<?php

namespace spec\Amneale\Ledger;

use PhpSpec\ObjectBehavior;
use Amneale\Ledger\Type;

class TypeSpec extends ObjectBehavior
{
    function it_should_be_initializable_with_type_credit()
    {
        $this->beConstructedWith(Type::CREDIT);
        $this->isCredit()->shouldReturn(true);
    }

    function it_should_be_initializable_with_type_debit()
    {
        $this->beConstructedWith(Type::DEBIT);
        $this->isCredit()->shouldReturn(false);
    }

    function it_should_not_be_initializable_with_invalid_type()
    {
        $this->beConstructedWith('foobar');
        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
