<?php

namespace spec\Amneale\Ledger;

use PhpSpec\ObjectBehavior;

class DescriptionSpec extends ObjectBehavior
{
    const TEST_DESCRIPTION = 'test description';

    function it_can_be_represented_as_a_string()
    {
        $this->beConstructedWith(self::TEST_DESCRIPTION);
        $this->__toString()->shouldReturn(self::TEST_DESCRIPTION);
    }
}
