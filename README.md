# Ledger 
[![Build status](https://img.shields.io/travis/amneale/ledger.svg?style=flat-square)](https://travis-ci.org/amneale/ledger)
[![Code coverage](https://img.shields.io/codecov/c/github/amneale/ledger.svg?style=flat-square)](https://codecov.io/gh/amneale/ledger)
[![Packagist](https://img.shields.io/packagist/v/amneale/ledger.svg?style=flat-square)](https://packagist.org/packages/amneale/ledger)

A set of classes for handling personal ledger transactions

## Install
Via Composer
``` bash
$ composer require amneale/ledger
```

## Testing
This library uses [phpspec](http://www.phpspec.net)for testing.
``` bash
$ vendor/bin/phpspec run
```
