<?php

namespace Amneale\Ledger;

interface Entity
{
    /**
     * @return Identity
     */
    public function getId(): Identity;
}
