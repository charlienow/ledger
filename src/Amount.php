<?php

namespace Amneale\Ledger;

use Assert\Assert;

final class Amount
{
    /**
     * @var int
     */
    private $amount;

    public function __construct(int $amount)
    {
        Assert::that($amount)->greaterThan(0);

        $this->amount = $amount;
    }

    public function asInt(): int
    {
        return $this->amount;
    }
}
