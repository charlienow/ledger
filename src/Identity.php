<?php

namespace Amneale\Ledger;

use Assert\Assert;
use Ramsey\Uuid\Uuid;

final class Identity
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id = null)
    {
        if (null === $id) {
            $id = $this->generateId();
        }

        Assert::that($id)->uuid();
        $this->id = $id;
    }

    public function asString(): string
    {
        return $this->id;
    }

    private function generateId(): string
    {
        return Uuid::uuid4()->toString();
    }
}
