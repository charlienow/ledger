<?php

namespace Amneale\Ledger;

use Assert\Assert;

final class Type
{
    const CREDIT = 'credit';
    const DEBIT = 'debit';

    const AVAILABLE_TYPES = [self::CREDIT, self::DEBIT];

    /**
     * @var string
     */
    private $type;

    public function __construct(string $type)
    {
        Assert::that($type)->choice(self::AVAILABLE_TYPES);

        $this->type = $type;
    }

    public function isCredit(): bool
    {
        return self::CREDIT === $this->type;
    }
}
