<?php

namespace Amneale\Ledger;

class Transaction implements Entity
{
    /**
     * @var Identity
     */
    private $id;

    /**
     * @var Amount
     */
    private $amount;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var Description
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $date;

    public function __construct(
        Identity $id,
        Amount $amount,
        Type $type,
        Description $description,
        \DateTime $date
    ) {
        $this->id = $id;
        $this->amount = $amount;
        $this->type = $type;
        $this->description = $description;
        $this->date = $date;
    }

    public static function create(
        Amount $amount,
        Type $type,
        Description $description,
        \DateTime $date
    ): self {
        return new static(new Identity(), $amount, $type, $description, $date);
    }

    public function getId(): Identity
    {
        return $this->id;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function isCredit(): bool
    {
        return $this->type->isCredit();
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }
}
